import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { getDDClasses } from './redux/actions/DDActions'
import { getSWPlanets, getSWHeroes } from './redux/actions/SWActions'
import logo from './logo.svg';
import './App.css';
import List from './components/organisms/List/List';

class App extends Component {
  render() {
    const { getSWHeroes, getSWPlanets } = this.props;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Nerd List</h1>
        </header>
        <div className="app-body">
        <List reducer='heroes' title='Heroes' getData={getSWHeroes}/>
        <List reducer='planets' title='Planets' getData={getSWPlanets}/>
        </div>
      </div>
    );
  }
}

export default connect(null, { getSWHeroes, getSWPlanets })(App);
