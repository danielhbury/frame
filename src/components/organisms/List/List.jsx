import React, { Component } from 'react';
import { connect } from 'react-redux';
import ListItem from '../../atoms/ListItem/ListItem';

class List extends Component {
  constructor(props) {
    super(props);

    props.getData();
  }

  render() {
    const { data, reducer, title } = this.props;
    const dataMap = data.length ? data.map((e, i) => {
      return <ListItem idx={i} type={reducer} key={i} />
    }) : null;
    return (
      <div className="list-container">
        <h1>{title}</h1>
        {dataMap}
      </div>
    )
  }
}

function mapStateToProps(state, ownProps) {
  const { reducer } = ownProps;
  return {
    data: state.starWars[reducer],
  }
}

export default connect(mapStateToProps, {})(List);