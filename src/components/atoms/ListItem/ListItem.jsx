import React from 'react'
import { connect } from 'react-redux';

const ListItem = (props) => {
  const {item} = props;
  return (
    <div className="list-item">
      {item}
    </div>
  )
}

function mapStateToProps(state, ownProps) {
  const { idx, type } = ownProps;
  return {
    item: state.starWars[type][idx].name,
  }
}

export default connect(mapStateToProps, {})(ListItem);