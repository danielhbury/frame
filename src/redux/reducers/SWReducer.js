import * as types from '../constants/types';

const initialState = {
  planets: [],
  planetsMap: {},
  heroes: [],
  heroesMap: {},
}

export default function (state = initialState, action) {
  switch (action.type) {
    case types.SET_STAR_WARS_PLANETS:
      return {
        ...state,
        planets: action.payload.data,
        planetsMap: action.payload.dataMap
      };
    case types.SET_STAR_WARS_HEROES:
      return {
        ...state,
        heroes: action.payload.data,
        heroesMap: action.payload.dataMap
      };
    default:
      return state;
  }
}