import { combineReducers } from 'redux';

import SWReducer from './reducers/SWReducer';

export const rootReducer = combineReducers({
  starWars: SWReducer,
})

export default rootReducer;