import * as types from '../constants/types';

export const getSWPlanets = () => (dispatch) => {
  dispatch({
    type: types.API,
    payload: {
      method: 'get',
      path: 'planets',
      success: ({ results }) => setSWPlanets(results),
    },
  })
}

export const setSWPlanets = (payload) => ({
  type: types.SET_STAR_WARS_PLANETS,
  payload: {
    data: payload
  },
})

export const getSWHeroes = () => (dispatch) => {
  dispatch({
    type: types.API,
    payload: {
      method: 'get',
      path: 'people',
      success: ({ results }) => setSWHeroes(results),
    },
  })
}

export const setSWHeroes = (payload) => ({
  type: types.SET_STAR_WARS_HEROES,
  payload: {
    data: payload,
  }
})