const mapData = () => next => action => {

  if (!Array.isArray(action.payload.data)) {
    return next(action);
  }

  const map = {};
  action.payload.data.forEach((item, i) => {
    map[item.name] = i
  })
  action.payload.dataMap = map;
  next(action);
}

export default mapData;