import axios from 'axios';
import * as types from '../constants/types';

const baseURL = 'https://swapi.co/api/';

const api = ({ dispatch }) => next => action => {

  if (action.type !== types.API) {
    return next(action);
  }
  const { method, path, options, success } = action.payload;

  return axios[method](`${baseURL}${path}`, options && options)
    .then((res) => {
      dispatch(success(res.data))
    })
    .catch((err) => {
      console.error(err);
    })
}

export default api
