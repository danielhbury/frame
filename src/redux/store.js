import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from './';
import api from './middleware/api';
import mapData from './middleware/mapData';

export default createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk, mapData, api),
);
